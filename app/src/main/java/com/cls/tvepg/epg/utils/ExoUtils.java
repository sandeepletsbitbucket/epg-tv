package com.cls.tvepg.epg.utils;

import android.content.Context;
import android.net.Uri;

import com.google.android.exoplayer2.C;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
//import com.google.android.exoplayer2.source.dash.DashMediaSource;
//import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;

public class ExoUtils {
    public static MediaSource buildMediaSource(Context context, Uri uri,
                                               DefaultBandwidthMeter defaultBandwidthMeter) {
        int type = Util.inferContentType(uri);
        switch (type) {
//            case C.TYPE_SS:
//                return new SsMediaSource(uri, new DefaultSsChunkSource.Factory(buildDataSourceFactory(context, true, defaultBandwidthMeter)),null,null);
//            case C.TYPE_DASH:
//                return new DashMediaSource(uri, buildDataSourceFactory(context, false, defaultBandwidthMeter),
//                        new DefaultDashChunkSource.Factory(buildDataSourceFactory(context, true, defaultBandwidthMeter))
//                        , null, null);
            case C.TYPE_HLS:
                return new HlsMediaSource(uri, buildDataSourceFactory(context, true, defaultBandwidthMeter), null, null);
            case C.TYPE_OTHER:
                return new ExtractorMediaSource(uri, buildDataSourceFactory(context, true, defaultBandwidthMeter), new DefaultExtractorsFactory(),
                        null, null);
            default: {
                throw new IllegalStateException("Unsupported type: " + type);
            }
        }
    }


    private static DataSource.Factory buildDataSourceFactory(Context context, boolean useBandwidthMeter,
                                                             DefaultBandwidthMeter defaultBandwidthMeter) {
        return buildDataSourceFactory(context, useBandwidthMeter ? defaultBandwidthMeter : null);
    }

    private static DataSource.Factory buildDataSourceFactory(Context context, DefaultBandwidthMeter bandwidthMeter) {
        return new DefaultDataSourceFactory(context, bandwidthMeter,
                buildHttpDataSourceFactory(context, bandwidthMeter));
    }

    private static HttpDataSource.Factory buildHttpDataSourceFactory(Context context,
                                                                     DefaultBandwidthMeter bandwidthMeter) {
        return new DefaultHttpDataSourceFactory(Util.getUserAgent(context, "MultitvPlayer"), bandwidthMeter);
    }
}