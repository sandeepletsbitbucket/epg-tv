
package com.cls.tvepg.epg.model.channel;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChannelModel implements Serializable
{

    @SerializedName("code")
    @Expose
    public Integer code;
    @SerializedName("result")
    @Expose
    public Result result;
    private final static long serialVersionUID = 7445502545287750691L;

}
