package com.cls.tvepg.epg.misc;

import android.util.Log;

import com.cls.tvepg.epg.model.latest.Channel;
import com.cls.tvepg.epg.model.latest.Epg_;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.cls.tvepg.epg.EPG;
//import com.cls.tvepg.epg.domain.EPGChannel;
//import com.cls.tvepg.epg.domain.EPGEvent;
import com.cls.tvepg.epg.utils.Utilities;

/**
 * Created by Kristoffer on 15-05-24.
 */
public class MockDataService {

    private static String TAG = "MockDataService";

//    private static Random rand = new Random();


//    private static List<Integer> availableEventLength = Lists.newArrayList(
//            1000 * 60 * 15,  // 15 minutes
//            1000 * 60 * 30,  // 30 minutes
//            1000 * 60 * 45,  // 45 minutes
//            1000 * 60 * 60,  // 60 minutes
//            1000 * 60 * 120  // 120 minutes
//    );
//
//    private static List<String> availableEventTitles = Lists.newArrayList(
//            "Avengers",
//            "How I Met Your Mother",
//            "Silicon Valley",
//            "Late Night with Jimmy Fallon",
//            "The Big Bang Theory",
//            "Leon",
//            "Die Hard"
//    );

//    private static List<String> availableChannelLogos = Lists.newArrayList(
//            "http://kmdev.se/epg/1.png",
//            "http://kmdev.se/epg/2.png",
//            "http://kmdev.se/epg/3.png",
//            "http://kmdev.se/epg/4.png",
//            "http://kmdev.se/epg/5.png"
//    );

//    public static Map<EPGChannel, List<EPGEvent>> getMockData() {
//        HashMap<EPGChannel, List<EPGEvent>> result = Maps.newLinkedHashMap();
//
//        long nowMillis = System.currentTimeMillis();
//
//        for (int i = 0; i < 20; i++) {
//            EPGChannel epgChannel = new EPGChannel(availableChannelLogos.get(i % 5),
//                    "Channel " + (i + 1), Integer.toString(i), null);
//
//            result.put(epgChannel, createEvents(epgChannel, nowMillis));
//        }
//
//        return result;
//    }


//    public static Map<EPGChannel, List<EPGEvent>> getMockData(List<Result> resultList) {
//        HashMap<EPGChannel, List<EPGEvent>> result = Maps.newLinkedHashMap();
//
//        long nowMillis = System.currentTimeMillis();
//
//        for (int i = 0; i < resultList.size(); i++) {
//
//            EPGChannel epgChannel = new EPGChannel(resultList.get(i).getChannel_logo(),
//                    resultList.get(i).getChannel(), Integer.toString(i));
//
//            result.put(epgChannel, createEvents(epgChannel, nowMillis, resultList.get(i)));
//        }
//
//        return result;
//    }


//    public static Map<EPGChannel, List<EPGEvent>> getMockData(List<Result> resultList, EPG epg) {
//        HashMap<EPGChannel, List<EPGEvent>> result = Maps.newLinkedHashMap();
//
//        long nowMillis = System.currentTimeMillis();
//
//        for (int i = 0; i < resultList.size(); i++) {
//
//            EPGChannel epgChannel = new EPGChannel(resultList.get(i).getChannel_logo(),
//                    resultList.get(i).getChannel(), Integer.toString(i), resultList.get(i).getEpg());
//
//            result.put(epgChannel, createEvents(epgChannel, nowMillis, resultList.get(i), epg));
//        }
//
//        return result;
//    }
//

    public static Map<Channel, ArrayList<Epg_>> getMockData1(ArrayList<Channel> resultList, EPG epg) {
        HashMap<Channel, ArrayList<Epg_>> result = Maps.newLinkedHashMap();

        long nowMillis = System.currentTimeMillis();

        for (int i = 0; i < resultList.size(); i++) {

            result.put(resultList.get(i), createEvents1(nowMillis, resultList.get(i), epg));
        }

        return result;
    }


    private static ArrayList<Epg_> createEvents1(long nowMillis, Channel result1, EPG epg) {
        ArrayList<Epg_> result = Lists.newArrayList();

        long ms = 0;
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        String formattedDate = df.format(c.getTime());

        Log.d(TAG, "formatted time passed===" + formattedDate);

        Pattern p = Pattern.compile("(\\d+):(\\d+)");
        Matcher m = p.matcher(formattedDate);
        if (m.matches()) {
            int hrs = Integer.parseInt(m.group(1));
            int min = Integer.parseInt(m.group(2));
            ms = (long) hrs * 60 * 60 * 1000 + min * 60 * 1000;
            Log.d(TAG, "hrs=" + hrs + " min=" + min + " ms=" + ms);
        }


        epg.setDaysBackMillis((int) ms);


        long epgStart = nowMillis - ms;
        long epgEnd = nowMillis + EPG.DAYS_FORWARD_MILLIS;


        long currentTime = epgStart;


        if (result1.getEpgs().get(0).getEpg()!=null && result1.getEpgs().get(0).getEpg().size()>0) {
            for (int i = 0; i < result1.getEpgs().get(0).getEpg().size(); i++) {
                ArrayList<Epg_> epgMetaDataList = result1.getEpgs().get(0).getEpg();

                Epg_ epgMetaData = epgMetaDataList.get(i);

                long eventEnd = getEventEnd(currentTime, epgMetaDataList.get(i).getDuration());

                epgMetaData.setStart(currentTime);
                epgMetaData.setEnd(eventEnd);
                result.add(epgMetaData);
                currentTime = eventEnd;
            }
        }

        return result;
    }


//    private static List<EPGEvent> createEvents(EPGChannel epgChannel, long nowMillis, Result result1, EPG epg) {
//        List<EPGEvent> result = Lists.newArrayList();
//
//
//        long ms = 0;
//        Calendar c = Calendar.getInstance();
//        System.out.println("Current time => " + c.getTime());
//
//        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
//        String formattedDate = df.format(c.getTime());
//
//        Log.d(TAG, "formatted time passed===" + formattedDate);
//
//        Pattern p = Pattern.compile("(\\d+):(\\d+)");
//        Matcher m = p.matcher(formattedDate);
//        if (m.matches()) {
//            int hrs = Integer.parseInt(m.group(1));
//            int min = Integer.parseInt(m.group(2));
//            ms = (long) hrs * 60 * 60 * 1000 + min * 60 * 1000;
//            Log.d(TAG, "hrs=" + hrs + " min=" + min + " ms=" + ms);
//        }
//
//
//        epg.setDaysBackMillis((int) ms);
//
//
//        long epgStart = nowMillis - ms;
//        long epgEnd = nowMillis + EPG.DAYS_FORWARD_MILLIS;
//
//
//        long currentTime = epgStart;
//
//
//        for (int i = 0; i < result1.getEpg().size(); i++) {
//
//            List<EpgMetaData> epgMetaDataList = result1.getEpg();
//
//            long eventEnd = getEventEnd(currentTime, epgMetaDataList.get(i).getDuration());
//
//
//            EPGEvent epgEvent = new EPGEvent(currentTime, eventEnd, epgMetaDataList.get(i).getTitle(), epgMetaDataList.get(i).getLogo(), epgMetaDataList.get(i).getUrl());
//            result.add(epgEvent);
//            currentTime = eventEnd;
//        }
//
//        return result;
//    }

//    private static List<EPGEvent> createEvents(EPGChannel epgChannel, long nowMillis) {
//        List<EPGEvent> result = Lists.newArrayList();
//
//        long epgStart = nowMillis - EPG.DAYS_BACK_MILLIS;
//        long epgEnd = nowMillis + EPG.DAYS_FORWARD_MILLIS;
//
//        long currentTime = epgStart;
//
//        while (currentTime <= epgEnd) {
//            long eventEnd = getEventEnd(currentTime);
//            EPGEvent epgEvent = new EPGEvent(currentTime, eventEnd, availableEventTitles.get(randomBetween(0, 6)), "", "");
//            result.add(epgEvent);
//            currentTime = eventEnd;
//        }
//
//        return result;
//    }


    private static long getEventEnd(long eventStartMillis, int duration) {


        long length = duration * 60 * 1000;
        return eventStartMillis + length;
    }

//    private static long getEventEnd(long eventStartMillis) {
//        long length = availableEventLength.get(randomBetween(0, 4));
//        return eventStartMillis + length;
//    }

//    private static int randomBetween(int start, int end) {
//        return start + rand.nextInt((end - start) + 1);
//    }
}
