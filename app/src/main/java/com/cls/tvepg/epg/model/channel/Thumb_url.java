
package com.cls.tvepg.epg.model.channel;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Thumb_url implements Serializable
{

    @SerializedName("base_path")
    @Expose
    public String base_path;
    @SerializedName("thumb_path")
    @Expose
    public String thumb_path;
    private final static long serialVersionUID = -333884234038062755L;

}
