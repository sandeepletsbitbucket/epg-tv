package com.cls.tvepg.epg.model.latest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Epg_ implements Serializable {

    @SerializedName("airtime")
    @Expose
    private String airtime;
    @SerializedName("channel")
    @Expose
    private String channel;
    @SerializedName("channel_no")
    @Expose
    private Integer channel_no;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("progdate")
    @Expose
    private String progdate;
    @SerializedName("progtime")
    @Expose
    private String progtime;
    @SerializedName("synopsis")
    @Expose
    private String synopsis;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("trip")
    @Expose
    private Integer trip;
    @SerializedName("url")
    @Expose
    private String url;

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    private  long start;
    private  long end;

    private final static long serialVersionUID = 2052982238078503898L;

    public String getAirtime() {
        return airtime;
    }

    public void setAirtime(String airtime) {
        this.airtime = airtime;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Integer getChannel_no() {
        return channel_no;
    }

    public void setChannel_no(Integer channel_no) {
        this.channel_no = channel_no;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getProgdate() {
        return progdate;
    }

    public void setProgdate(String progdate) {
        this.progdate = progdate;
    }

    public String getProgtime() {
        return progtime;
    }

    public void setProgtime(String progtime) {
        this.progtime = progtime;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getTrip() {
        return trip;
    }

    public void setTrip(Integer trip) {
        this.trip = trip;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    public boolean isCurrent() {
        long now = System.currentTimeMillis();
        return now >= start && now <= end;
    }
}