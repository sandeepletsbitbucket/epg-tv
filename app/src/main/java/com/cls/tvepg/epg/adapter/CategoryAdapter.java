package com.cls.tvepg.epg.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cls.tvepg.R;
import com.cls.tvepg.epg.model.latest.ResultGenre;

import java.util.ArrayList;


/**
 * Created by arungoyal on 01/06/17.
 */

public class CategoryAdapter extends ArrayAdapter<ResultGenre>{
    LayoutInflater layoutInflater;
    ArrayList<ResultGenre> mList;
    ArrayList<ResultGenre> genreList;
    public CategoryAdapter(Context context, int resource, ArrayList<ResultGenre> list) {
        super(context, resource);
        layoutInflater = LayoutInflater.from(context);
        mList = list;

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.category_item, null);
        }
        TextView textView = view.findViewById(R.id.categoryName_tv);
        textView.setText(mList.get(position).genre_name);

        return view;
    }
}
