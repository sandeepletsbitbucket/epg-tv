package com.cls.tvepg.epg.model.latest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class GenreModel implements Serializable
{

@SerializedName("code")
@Expose
public Integer code;
@SerializedName("result")
@Expose
public ArrayList<ResultGenre> result = null;
@SerializedName("total")
@Expose
public String total;
private final static long serialVersionUID = 3189505518095722049L;

}