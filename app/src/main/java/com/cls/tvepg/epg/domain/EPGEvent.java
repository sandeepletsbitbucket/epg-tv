//package com.cls.tvepg.epg.domain;
//
//import java.io.Serializable;
//
///**
// * Created by Kristoffer.
// */
//public class EPGEvent implements Serializable{
//
//    private final long start;
//    private final long end;
//    private final String title;
//    private final String logo;
//    private final String url;
//
//    public EPGEvent(long start, long end, String title, String logo,String url) {
//        this.start = start;
//        this.end = end;
//        this.title = title;
//        this.logo=logo;
//        this.url=url;
//    }
//
//
//    public String getUrl() {
//        return url;
//    }
//
//    public String getLogo() {
//        return logo;
//    }
//
//    public long getStart() {
//        return start;
//    }
//
//    public long getEnd() {
//        return end;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public boolean isCurrent() {
//        long now = System.currentTimeMillis();
//        return now >= start && now <= end;
//    }
//}
