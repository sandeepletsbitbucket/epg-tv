package com.cls.tvepg.epg.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference {

    public String PREFS_NAME = "epgtv";

    public SharedPreference() {
        super();
    }

    public void setPreferencesString(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreferencesString(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void clearPrefs(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }
}