package com.cls.tvepg.epg.utils;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.cls.tvepg.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by arungoyal on 30/05/17.
 */

public class Utilities {


    public static String convert(String time) {

        String result = null;
        DateFormat format1 = new SimpleDateFormat("hh:mm:ss");
        try {
            Date date = format1.parse(time);
            SimpleDateFormat format2 = new SimpleDateFormat("hh:mm");
            result = format2.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return result;
    }


    public static String convertTime(String time) {

        String fomattedTime = null;
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");


        try {

            Date date = formatter.parse(time);
            System.out.println(date);
            fomattedTime = formatter.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return fomattedTime;
    }


    public static void applyFontForToolbarTitle(Activity context) {

        Toolbar toolbar = (Toolbar) context.findViewById(R.id.toolbar);
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;

                Typeface tf = null;

                tf = Typeface.createFromAsset(context.getAssets(),
                        "Montserrat-Regular.ttf");

                tv.setTypeface(tf);
            }
        }
    }

    public static long dayToMillis(int days) {

        return days * 24 * 60 * 60 * 1000;
    }

    public static long hoursToMillis(int hours) {

        return hours * 60 * 60 * 1000;
    }

    public static long minsToMillis(int mins) {

        return mins * 60 * 1000;
    }


    public static int millisToDays(long millis) {
        return (int) millis / 24 * 60 * 60 * 1000;
    }

    public static int millisToHours(long millis) {
        return (int) millis / 60 * 60 * 1000;
    }

    public static int millisToMins(long millis) {
        return (int) millis / 60 * 1000;
    }


    private static void test(int nowHour, int nowMin, String endTime) {
        Matcher m = Pattern.compile("(\\d{2}):(\\d{2})").matcher(endTime);
        if (!m.matches())
            throw new IllegalArgumentException("Invalid time format: " + endTime);
        int endHour = Integer.parseInt(m.group(1));
        int endMin = Integer.parseInt(m.group(2));
        if (endHour >= 24 || endMin >= 60)
            throw new IllegalArgumentException("Invalid time format: " + endTime);
        int minutesLeft = endHour * 60 + endMin - (nowHour * 60 + nowMin);
        if (minutesLeft < 0)
            minutesLeft += 24 * 60; // Time passed, so time until 'end' tomorrow
        int hours = minutesLeft / 60;
        int minutes = minutesLeft - hours * 60;
        String str = hours + " : " + minutes;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(str);


        System.out.println(hours + "h " + minutes + "m until " + endTime);
    }


}
