
package com.cls.tvepg.epg.model.channel;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Serializable
{

    @SerializedName("languages")
    @Expose
    public List<String> languages = null;
    @SerializedName("live")
    @Expose
    public List<Live> live = null;
    @SerializedName("offset")
    @Expose
    public Integer offset;
    @SerializedName("totalcount")
    @Expose
    public Integer totalcount;
    @SerializedName("version")
    @Expose
    public String version;
    private final static long serialVersionUID = -398643801948955466L;

}
