package com.cls.tvepg.epg.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cls.tvepg.R;
import com.cls.tvepg.epg.utils.NotificationCenter;


/**
 * Created by arungoyal on 01/06/17.
 */


public class ChannelViewHolder extends RecyclerView.ViewHolder {


    public ImageView channelImg;

    public ChannelViewHolder(View itemView) {
        super(itemView);

        channelImg = (ImageView) itemView.findViewById(R.id.channelImg);
    }


}