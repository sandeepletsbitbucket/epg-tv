package com.cls.tvepg.epg.misc;

import com.cls.tvepg.epg.model.EpgMetaData;
import com.cls.tvepg.epg.model.Result;
import com.cls.tvepg.epg.model.latest.Channel;
import com.cls.tvepg.epg.model.latest.Epg_;
import com.google.common.collect.Lists;
import com.cls.tvepg.epg.EPGData;
//import com.cls.tvepg.epg.domain.EPGChannel;
//import com.cls.tvepg.epg.domain.EPGEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * TODO: Add null check when fetching at position etc.
 * Created by Kristoffer on 15-05-23.
 */
public class EPGDataImpl implements EPGData {

//    private List<EPGChannel> channels = Lists.newArrayList();
//    private List<List<EPGEvent>> events = Lists.newArrayList();



    private ArrayList<Channel> channels1 = Lists.newArrayList();
    private ArrayList<ArrayList<Epg_>> events1 = Lists.newArrayList();

//    public EPGDataImpl(Map<EPGChannel, List<EPGEvent>> data) {
//        channels = Lists.newArrayList(data.keySet());
//        events = Lists.newArrayList(data.values());
//    }


    public EPGDataImpl(Map<Channel, ArrayList<Epg_>> data) {
        channels1 = Lists.newArrayList(data.keySet());
        events1 = Lists.newArrayList(data.values());
    }

    public Channel getChannel(int position) {
        return channels1.get(position);
    }

    public ArrayList<Epg_> getEvents(int channelPosition) {
        return events1.get(channelPosition);
    }

    public Epg_ getEvent(int channelPosition, int programPosition) {
        return events1.get(channelPosition).get(programPosition);
    }

    public int getChannelCount() {
        return channels1.size();
    }

    @Override
    public boolean hasData() {
        return !channels1.isEmpty();
    }
}
