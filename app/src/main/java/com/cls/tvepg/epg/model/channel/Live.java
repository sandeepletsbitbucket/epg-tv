
package com.cls.tvepg.epg.model.channel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Live implements Serializable
{

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("title")
    @Expose
    public String title;
    @SerializedName("channelid")
    @Expose
    public String channelid;
    @SerializedName("channel_number")
    @Expose
    public String channel_number;
    @SerializedName("content_partner")
    @Expose
    public Object content_partner;
    @SerializedName("description")
    @Expose
    public String description;
    @SerializedName("keywords")
    @Expose
    public List<String> keywords = null;
    @SerializedName("language_id")
    @Expose
    public String language_id;
    @SerializedName("language")
    @Expose
    public String language;
    @SerializedName("des")
    @Expose
    public String des;
    @SerializedName("publish_date")
    @Expose
    public String publish_date;
    @SerializedName("category_id")
    @Expose
    public String category_id;
    @SerializedName("category")
    @Expose
    public String category;
    @SerializedName("url")
    @Expose
    public String url;
    @SerializedName("genres")
    @Expose
    public ArrayList<String> genres = null;
    @SerializedName("media_type")
    @Expose
    public String media_type;
    @SerializedName("price_type")
    @Expose
    public String price_type;
    @SerializedName("likes_count")
    @Expose
    public String likes_count;
    @SerializedName("rating")
    @Expose
    public String rating;
    @SerializedName("watch")
    @Expose
    public String watch;
    @SerializedName("favorite")
    @Expose
    public String favorite;
    @SerializedName("expiry_date")
    @Expose
    public String expiry_date;
    @SerializedName("app_id")
    @Expose
    public String app_id;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("ishd")
    @Expose
    public String ishd;
    @SerializedName("iscatchup")
    @Expose
    public String iscatchup;
    @SerializedName("likes")
    @Expose
    public String likes;
    @SerializedName("secureurl")
    @Expose
    public Boolean secureurl;
    @SerializedName("Playurl")
    @Expose
    public String playurl;
    @SerializedName("Shareurl")
    @Expose
    public String shareurl;
    @SerializedName("thumbnail")
    @Expose
    public Thumbnail thumbnail;
    @SerializedName("thumb_url")
    @Expose
    public Thumb_url thumb_url;
    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("Drm")
    @Expose
    public String drm;
    @SerializedName("Index")
    @Expose
    public String index;
    @SerializedName("source")
    @Expose
    public String source;
    @SerializedName("duration")
    @Expose
    public String duration;
    @SerializedName("preview_url")
    @Expose
    public String preview_url;
    @SerializedName("meta")
    @Expose
    public Meta meta;
    @SerializedName("category_ids")
    @Expose
    public List<Object> category_ids = null;
    @SerializedName("current_date")
    @Expose
    public String current_date;
    @SerializedName("package_info")
    @Expose
    public List<Object> package_info = null;
    @SerializedName("payperview_info")
    @Expose
    public List<Object> payperview_info = null;
    private final static long serialVersionUID = -7588991250538021373L;

}
