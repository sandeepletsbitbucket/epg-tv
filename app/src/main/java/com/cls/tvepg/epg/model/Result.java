package com.cls.tvepg.epg.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by arungoyal on 30/05/17.
 */

public class Result implements Serializable {

    private String channel;
    private String channel_logo;
    private String date;
    private List<EpgMetaData> epg;
    private String stream_url;

    public String getStream_url() {
        return stream_url;
    }

    public void setStream_url(String stream_url) {
        this.stream_url = stream_url;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getChannel_logo() {
        return channel_logo;
    }

    public void setChannel_logo(String channel_logo) {
        this.channel_logo = channel_logo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<EpgMetaData> getEpg() {
        return epg;
    }

    public void setEpg(List<EpgMetaData> epg) {
        this.epg = epg;
    }
}
