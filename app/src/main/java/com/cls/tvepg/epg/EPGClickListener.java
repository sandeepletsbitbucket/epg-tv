package com.cls.tvepg.epg;

//import com.cls.tvepg.epg.domain.EPGChannel;
//import com.cls.tvepg.epg.domain.EPGEvent;
import com.cls.tvepg.epg.model.EpgMetaData;
import com.cls.tvepg.epg.model.Result;
import com.cls.tvepg.epg.model.latest.Channel;
import com.cls.tvepg.epg.model.latest.Epg_;

/**
 * Created by Kristoffer on 15-05-25.
 */
public interface EPGClickListener {

    void onChannelClicked(int channelPosition, Channel epgChannel);

    void onEventClicked(int channelPosition, int programPosition, Epg_ epgEvent);

    void onResetButtonClicked();
}
