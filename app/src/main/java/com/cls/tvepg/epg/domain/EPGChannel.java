//package com.cls.tvepg.epg.domain;
//
//import com.cls.tvepg.epg.model.EpgMetaData;
//
//import java.util.List;
//
///**
// * Created by Kristoffer.
// */
//public class EPGChannel {
//
//    private final String channelID;
//    private final String name;
//    private final String imageURL;
//    private final List<EpgMetaData> epg;
//
//    public EPGChannel(String imageURL, String name, String channelID, List<EpgMetaData> epg) {
//        this.imageURL = imageURL;
//        this.name = name;
//        this.channelID = channelID;
//        this.epg=epg;
//    }
//
//
//    public List<EpgMetaData> getEpg() {
//        return epg;
//    }
//
//    public String getChannelID() {
//        return channelID;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getImageURL() {
//        return imageURL;
//    }
//}
