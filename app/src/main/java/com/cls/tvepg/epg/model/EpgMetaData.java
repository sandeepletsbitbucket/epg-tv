package com.cls.tvepg.epg.model;

import java.io.Serializable;

/**
 * Created by arungoyal on 30/05/17.
 */


public class EpgMetaData implements Serializable{


    private int _id;
    private String language;
    private String title;
    private String channel_logo;
    private String progtime;
    private String airtime;
    private int duration;
    private String synopsis;
    private int channel_no;
    private int trp;
    private String progdate;
    private String logo;
    private String channel;
    private String provider;
    private  long start;
    private  long end;
    private String url;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getChannel_logo() {
        return channel_logo;
    }

    public void setChannel_logo(String channel_logo) {
        this.channel_logo = channel_logo;
    }

    public String getProgtime() {
        return progtime;
    }

    public void setProgtime(String progtime) {
        this.progtime = progtime;
    }

    public String getAirtime() {
        return airtime;
    }

    public void setAirtime(String airtime) {
        this.airtime = airtime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String synopsis) {
        this.synopsis = synopsis;
    }

    public int getChannel_no() {
        return channel_no;
    }

    public void setChannel_no(int channel_no) {
        this.channel_no = channel_no;
    }

    public int getTrp() {
        return trp;
    }

    public void setTrp(int trp) {
        this.trp = trp;
    }

    public String getProgdate() {
        return progdate;
    }

    public void setProgdate(String progdate) {
        this.progdate = progdate;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }


    public boolean isCurrent() {
        long now = System.currentTimeMillis();
        return now >= start && now <= end;
    }
}
