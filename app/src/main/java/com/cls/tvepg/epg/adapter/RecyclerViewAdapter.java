package com.cls.tvepg.epg.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.cls.tvepg.PlayerActivity;
import com.cls.tvepg.R;
import com.cls.tvepg.epg.model.EpgMetaData;
import com.cls.tvepg.epg.model.Result;
import com.cls.tvepg.epg.model.latest.Channel;
import com.cls.tvepg.epg.utils.NotificationCenter;
import com.squareup.picasso.Picasso;

import java.util.List;


public class RecyclerViewAdapter extends RecyclerView.Adapter<ChannelViewHolder> {

    private List<Channel> itemList;
    private Context context;

    public RecyclerViewAdapter(Context context, List<Channel> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public ChannelViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.channel_layout, null);
        ChannelViewHolder rcv = new ChannelViewHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(ChannelViewHolder holder, final int position) {


        Picasso
                .with(context)
                .load(itemList.get(position).getThumbnail()/*.replaceAll(" ", "%20")*/)
                .into(holder.channelImg);


        holder.channelImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PlayerActivity.class);
                intent.putExtra("result", itemList.get(position));
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


}