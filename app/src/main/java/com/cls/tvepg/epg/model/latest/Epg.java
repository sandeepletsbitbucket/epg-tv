package com.cls.tvepg.epg.model.latest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import java.io.Serializable;

public class Epg implements Serializable
{

    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("epg")
    @Expose
    private ArrayList<Epg_> epg = null;
    private final static long serialVersionUID = 274751324838177171L;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<Epg_> getEpg() {
        return epg;
    }

    public void setEpg(ArrayList<Epg_> epg) {
        this.epg = epg;
    }
}