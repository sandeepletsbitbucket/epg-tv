
package com.cls.tvepg.epg.model.channel;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Thumbnail implements Serializable
{

    @SerializedName("large")
    @Expose
    public String large;
    @SerializedName("medium")
    @Expose
    public String medium;
    @SerializedName("small")
    @Expose
    public String small;
    private final static long serialVersionUID = 489803949728011810L;

}
