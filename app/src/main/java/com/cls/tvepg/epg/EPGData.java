package com.cls.tvepg.epg;

//import com.cls.tvepg.epg.domain.EPGEvent;

import java.util.ArrayList;
import java.util.List;

//import com.cls.tvepg.epg.domain.EPGChannel;
import com.cls.tvepg.epg.model.EpgMetaData;
import com.cls.tvepg.epg.model.Result;
import com.cls.tvepg.epg.model.latest.Channel;
import com.cls.tvepg.epg.model.latest.Epg_;

/**
 * Interface to implement and pass to EPG containing data to be used.
 * Implementation can be a simple as simple as a Map/List or maybe an Adapter.
 * Created by Kristoffer on 15-05-23.
 */
public interface EPGData {

    Channel getChannel(int position);

    ArrayList<Epg_> getEvents(int channelPosition);

    Epg_ getEvent(int channelPosition, int programPosition);

    int getChannelCount();

    boolean hasData();
}
