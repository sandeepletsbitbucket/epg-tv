package com.cls.tvepg.epg.model.latest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class Result implements Serializable
{
    @SerializedName("channels")
    @Expose
    private ArrayList<Channel> channels = null;
    @SerializedName("current_page")
    @Expose
    private Integer current_page;
    @SerializedName("nextPage_url")
    @Expose
    private String nextPage_url;
    @SerializedName("total_page")
    @Expose
    private Integer total_page;
    private final static long serialVersionUID = -9220844928001213818L;

    public ArrayList<Channel> getChannels() {
        return channels;
    }

    public void setChannels(ArrayList<Channel> channels) {
        this.channels = channels;
    }

    public Integer getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(Integer current_page) {
        this.current_page = current_page;
    }

    public String getNextPage_url() {
        return nextPage_url;
    }

    public void setNextPage_url(String nextPage_url) {
        this.nextPage_url = nextPage_url;
    }

    public Integer getTotal_page() {
        return total_page;
    }

    public void setTotal_page(Integer total_page) {
        this.total_page = total_page;
    }
}