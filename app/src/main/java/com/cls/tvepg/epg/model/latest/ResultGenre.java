package com.cls.tvepg.epg.model.latest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ResultGenre implements Serializable
{@SerializedName("id")
@Expose
public Integer id;
    @SerializedName("genre_name")
    @Expose
    public String genre_name;
    @SerializedName("thumb")
    @Expose
    public String thumb;
    private final static long serialVersionUID = 451494309481785646L;

}