package com.cls.tvepg.epg.model.latest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Channel implements Serializable
{
    @SerializedName("aid")
    @Expose
    private String aid;
    @SerializedName("name")
    @Expose
    private String name;

    private String url;
    private String thumbnail;
    private ArrayList<String> genres;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @SerializedName("epgs")
    @Expose
    private ArrayList<Epg> epgs = null;
    private final static long serialVersionUID = -591741902027560299L;

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Epg> getEpgs() {
        return epgs;
    }

    public void setEpgs(ArrayList<Epg> epgs) {
        this.epgs = epgs;
    }

    public void setGenres(ArrayList<String> genres) {
        this.genres = genres;
    }
    public ArrayList<String> getGenres() {
        return genres;
    }

}