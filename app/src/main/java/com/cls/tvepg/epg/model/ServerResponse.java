package com.cls.tvepg.epg.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by arungoyal on 30/05/17.
 */

public class ServerResponse {

    private int code;
    private ArrayList<Result> result;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }
}
