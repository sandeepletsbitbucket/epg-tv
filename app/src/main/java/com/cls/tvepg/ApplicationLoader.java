package com.cls.tvepg;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.cls.tvepg.epg.utils.LruBitmapCache;
import com.iainconnor.objectcache.CacheManager;
import com.iainconnor.objectcache.DiskCache;


import java.io.File;
import java.util.Locale;

/**
 * Created by Two on 28-11-2016.
 */


public class ApplicationLoader extends Application {
    public static Context applicationContext;
    public static final String TAG = ApplicationLoader.class
            .getSimpleName();
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    public static ApplicationLoader mInstance;
    private CacheManager cacheManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        applicationContext = getApplicationContext();
    }


    public CacheManager getCacheManager() {
        if (cacheManager == null) {
            try {
                File cacheFile = new File(getFilesDir() + File.separator + getPackageName());
                DiskCache diskCache = new DiskCache(cacheFile, BuildConfig.VERSION_CODE, 1024 * 1024 * 10);
                cacheManager = CacheManager.getInstance(diskCache);
            } catch (Exception e) {

                Log.e("cacheManager", "" + e.getMessage());
            }
        }

        return this.cacheManager;
    }


    public static synchronized ApplicationLoader getInstance() {
        return mInstance;
    }


    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


//    public boolean isRTL(Context ctx) {
//        Configuration config = ctx.getResources().getConfiguration();
//        return config.getLayoutDirection() == View.LAYOUT_DIRECTION_RTL;
//    }

    public static void updateLanguage(Context ctx, String lang) {
        Configuration cfg = new Configuration();
        if (!TextUtils.isEmpty(lang))
            cfg.locale = new Locale(lang);
        else
            cfg.locale = Locale.getDefault();

        ctx.getResources().updateConfiguration(cfg, null);
    }

}
