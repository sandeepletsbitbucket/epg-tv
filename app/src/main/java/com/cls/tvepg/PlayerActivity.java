package com.cls.tvepg;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cls.tvepg.epg.adapter.RecyclerViewAdapter;
//import com.cls.tvepg.epg.domain.EPGEvent;
//import com.cls.tvepg.epg.model.EpgMetaData;
//import com.cls.tvepg.epg.model.Result;
import com.cls.tvepg.epg.model.latest.Channel;
import com.cls.tvepg.epg.model.latest.Epg_;
import com.cls.tvepg.epg.model.latest.ServerResponseNew;
import com.cls.tvepg.epg.utils.ExoUtils;
import com.cls.tvepg.epg.utils.NotificationCenter;
import com.cls.tvepg.epg.utils.ScreenUtils;
import com.cls.tvepg.epg.utils.Utilities;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.gson.reflect.TypeToken;

import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class PlayerActivity extends AppCompatActivity implements NotificationCenter.NotificationCenterDelegate {
    private static final DefaultBandwidthMeter defaultBandwidthMeter = new DefaultBandwidthMeter();
    private SimpleExoPlayerView simpleExoPlayerViewPlayer;
    private String url;
    private int channelPosition;
    private TextView titleTxt, descTxt, titleTxtNext, descTxtNext, nextProgTime, currProgTime;
    private GridLayoutManager lLayout;
    private RecyclerView rView;
    private SimpleExoPlayer player;
    private String TAG = this.getClass().getSimpleName();
    private TrackSelector trackSelector;
    int currentPosition;
    private FrameLayout playerFarmeLayout;
    private ProgressBar progressBar2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player_screen);
        titleTxt = (TextView) findViewById(R.id.titleTxt);
        descTxt = (TextView) findViewById(R.id.descTxt);
        rView = (RecyclerView) findViewById(R.id.recycler_view);
        titleTxtNext = (TextView) findViewById(R.id.titleTxtNext);
        descTxtNext = (TextView) findViewById(R.id.descTxtNext);
        nextProgTime = (TextView) findViewById(R.id.nextProgTime);
        currProgTime = (TextView) findViewById(R.id.currProgTime);
        playerFarmeLayout = (FrameLayout) findViewById(R.id.playerFarmeLayout);
        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);
        Intent intent = getIntent();
        url = intent.getStringExtra("url");
        channelPosition = intent.getIntExtra("channelPosition", -1);


        final String key = "live";
        final Type homeObjectType = new TypeToken<ServerResponseNew>() {
        }.getType();

        ServerResponseNew channelObj = (ServerResponseNew) ApplicationLoader.getInstance().getCacheManager().get(key, ServerResponseNew.class, homeObjectType);


        NotificationCenter.getInstance().addObserver(this, NotificationCenter.channelClicked);

        lLayout = new GridLayoutManager(PlayerActivity.this, 3);
        rView.setLayoutManager(lLayout);

        playerFarmeLayout.setLayoutParams(new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, ScreenUtils.getScreenHeight(this) / 3 + 60));

        if (channelObj != null) {
            setRecyclerviewAndTitle(channelObj.getResult().getChannels());
        } else {
            Log.e("Channel Object status", "===null");
        }

        simpleExoPlayerViewPlayer = (SimpleExoPlayerView) findViewById(R.id.video_frame);
        TrackSelection.Factory videoTrackSelectionFactory = new AdaptiveTrackSelection.Factory(defaultBandwidthMeter);
        trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        simpleExoPlayerViewPlayer.setPlayer(player);
        player.setPlayWhenReady(true);
        MediaSource mediaSource = ExoUtils.buildMediaSource(this, Uri.parse(url), defaultBandwidthMeter);
        player.prepare(mediaSource);
        playerCallBack();

    }

    private void playerCallBack() {
        player.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {

            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                switch (playbackState) {
                    case 1:
                        break;
                    case 2:
                        progressBar2.setVisibility(View.VISIBLE);
                        break;
                    case 3:
                        progressBar2.setVisibility(View.GONE);
                        break;
                }
            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {

            }

            @Override
            public void onPositionDiscontinuity() {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }
        });
    }

    private void setRecyclerviewAndTitle(ArrayList<Channel> channels) {
        if (channels != null && channels.get(channelPosition) != null && channels.get(channelPosition).getEpgs().get(0).getEpg() != null && channels.get(channelPosition).getEpgs().get(0).getEpg().size() > 0) {
            Channel result = channels.get(channelPosition);
            List<Epg_> epgMetaDataList = result.getEpgs().get(0).getEpg();
            for (int i = 0; i < epgMetaDataList.size(); i++) {
                if (epgMetaDataList.get(i).isCurrent()) {
                    currentPosition = i;
                    Epg_ epgMetaData = epgMetaDataList.get(i);
                    Log.e(TAG, "Current epg name=====>>>> " + epgMetaData.getTitle());

                    Log.e(TAG, "Current epg name=====>>>> next" + epgMetaDataList.get(i + 1).getTitle());

                    Log.e(TAG, "fomatted time======>>>>" + Utilities.convertTime(epgMetaDataList.get(i).getProgtime()));

                    titleTxt.setText(epgMetaDataList.get(i).getTitle());
                    descTxt.setText(epgMetaDataList.get(i).getSynopsis());
                    currProgTime.setText(Utilities.convert(epgMetaDataList.get(i).getProgtime()) + " - " + Utilities.convert(epgMetaDataList.get(i + 1).getProgtime()));

                    titleTxtNext.setText(epgMetaDataList.get(i + 1).getTitle());
                    descTxtNext.setText(epgMetaDataList.get(i + 1).getSynopsis());
                    nextProgTime.setText(Utilities.convert(epgMetaDataList.get(i + 1).getProgtime()));

                    break;
                }
            }
        }

        Log.e(TAG, "player related item ====" + channels.size());

        RecyclerViewAdapter rcAdapter = new RecyclerViewAdapter(PlayerActivity.this, channels);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(rView.getContext(), lLayout.getOrientation());
        rView.addItemDecoration(dividerItemDecoration);
        rView.setAdapter(rcAdapter);
        rView.setNestedScrollingEnabled(false);
        rView.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.release();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Channel result = (Channel) intent.getSerializableExtra("result");
        List<Epg_> epgMetaDataList = result.getEpgs().get(0).getEpg();
        for (int i = 0; i < epgMetaDataList.size(); i++) {
            if (epgMetaDataList.get(i).isCurrent()) {
                currentPosition = i;
                Epg_ epgMetaData = epgMetaDataList.get(i);
                Log.d(TAG, "Current epg name=====>>>> " + epgMetaData.getTitle());

                Log.d(TAG, "Current epg name=====>>>> next" + epgMetaDataList.get(i + 1).getTitle());
                titleTxt.setText(epgMetaDataList.get(i).getTitle());
                descTxt.setText(epgMetaDataList.get(i).getSynopsis());
                currProgTime.setText(Utilities.convert(epgMetaDataList.get(i).getProgtime()) + " - " + Utilities.convert(epgMetaDataList.get(i + 1).getProgtime()));
                titleTxtNext.setText(epgMetaDataList.get(i + 1).getTitle());
                descTxtNext.setText(epgMetaDataList.get(i + 1).getSynopsis());
                nextProgTime.setText(Utilities.convert(epgMetaDataList.get(i + 1).getProgtime()));
                if (player != null) {
                    player.release();
                }
                player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
                simpleExoPlayerViewPlayer.setPlayer(player);
                player.setPlayWhenReady(true);
                MediaSource mediaSource = ExoUtils.buildMediaSource(this, Uri.parse(result.getUrl()), defaultBandwidthMeter);
                player.prepare(mediaSource);
                playerCallBack();

                break;
            }
        }


    }

    @Override
    public void didReceivedNotification(int id, Object... args) {

        if (id == NotificationCenter.channelClicked) {
            Intent intent = new Intent(PlayerActivity.this, PlayerActivity.class);
            startActivity(intent);
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            playerFarmeLayout.setLayoutParams(new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT, ScreenUtils.getScreenHeight(this) / 3 + 60));
        } else {
            playerFarmeLayout.setLayoutParams(new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
        }
    }
}