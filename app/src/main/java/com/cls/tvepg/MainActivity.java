package com.cls.tvepg;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.cls.tvepg.epg.EPG;
import com.cls.tvepg.epg.EPGClickListener;
import com.cls.tvepg.epg.EPGData;
import com.cls.tvepg.epg.adapter.CategoryAdapter;
//import com.cls.tvepg.epg.domain.EPGChannel;
//import com.cls.tvepg.epg.domain.EPGEvent;
import com.cls.tvepg.epg.misc.EPGDataImpl;
import com.cls.tvepg.epg.misc.MockDataService;
/*import com.cls.tvepg.epg.model.EpgMetaData;
import com.cls.tvepg.epg.model.Result;
import com.cls.tvepg.epg.model.ServerResponse;*/
import com.cls.tvepg.epg.model.channel.ChannelModel;
import com.cls.tvepg.epg.model.channel.Live;
import com.cls.tvepg.epg.model.latest.Channel;
import com.cls.tvepg.epg.model.latest.Epg_;
import com.cls.tvepg.epg.model.latest.GenreModel;
import com.cls.tvepg.epg.model.latest.ResultGenre;
import com.cls.tvepg.epg.model.latest.ServerResponseNew;
import com.cls.tvepg.epg.utils.GsonContextLoader;
import com.cls.tvepg.epg.utils.SharedPreference;
import com.cls.tvepg.epg.utils.Utilities;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class MainActivity extends AppCompatActivity {

    private String TAG = this.getClass().getSimpleName();
    private EPG epg;
    private ProgressBar progressBar2;

    private Toolbar toolbar;
    String formattedDate;
    private LinearLayout categoryLayout, languageLayout;
    //    private ArrayList<Result> results;
    RecyclerView rView;
    private GridLayoutManager lLayout;
    private ArrayList<Channel> channels = new ArrayList<>();
    private ChannelModel channelResponse;
    private ServerResponseNew serverResponse;
    private String genreUrl = "http://api-aut.multitvsolution.com/automatorapi/v6/content/getgenre/token/5d09d5cc77d03/device/web";
    private GenreModel genreModel;
    private SharedPreference prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        Utilities.applyFontForToolbarTitle(MainActivity.this);
        categoryLayout = (LinearLayout) findViewById(R.id.categoryLayout);
        languageLayout = (LinearLayout) findViewById(R.id.languageLayout);
        rView = (RecyclerView) findViewById(R.id.recycler_view);
        lLayout = new GridLayoutManager(MainActivity.this, 3);
        prefs = new SharedPreference();


       /* languageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showLanguageListing();
            }
        });
*/
        Calendar c = Calendar.getInstance();


        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c.getTime());

        progressBar2 = (ProgressBar) findViewById(R.id.progressBar2);
        //getChannelList(epgData);
        getBaseAndEPGURLs();


        epg = (EPG) findViewById(R.id.epg);


        epg.setEPGClickListener(new EPGClickListener() {
            @Override
            public void onChannelClicked(int channelPosition, Channel epgChannel) {
                if (channels.get(channelPosition).getUrl() != null && channels.get(channelPosition).getUrl().length() > 0) {

                    //  ArrayList<Epg_> epgMetaDataList = epgChannel.getEpgs().get(0).getEpg();
                    try {
                        ApplicationLoader.getInstance().getCacheManager().clear();
                        ApplicationLoader.getInstance().getCacheManager().put("live", null);
                        ApplicationLoader.getInstance().getCacheManager().put("live", serverResponse);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
                    intent.putExtra("channelPosition", channelPosition);
                    intent.putExtra("url", channels.get(channelPosition).getUrl());
                    //intent.putExtra("data", channels);
                    startActivity(intent);
                } else {
                    Toast.makeText(MainActivity.this, "Program is not available right now", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onEventClicked(int channelPosition, int programPosition, Epg_ epgEvent) {

                if (channels.get(channelPosition).getUrl() != null && channels.get(channelPosition).getUrl().length() > 0) {
                    try {
                        ApplicationLoader.getInstance().getCacheManager().clear();
                        ApplicationLoader.getInstance().getCacheManager().put("live", null);
                        ApplicationLoader.getInstance().getCacheManager().put("live", serverResponse);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Intent intent = new Intent(MainActivity.this, PlayerActivity.class);
                    intent.putExtra("url", channels.get(channelPosition).getUrl());
                    intent.putExtra("channelPosition", channelPosition);
                    //intent.putExtra("data", channels);
                    startActivity(intent);

                } else {
                    Toast.makeText(MainActivity.this, "Program is not available right now", Toast.LENGTH_LONG).show();
                }


            }

            @Override
            public void onResetButtonClicked() {
                epg.recalculateAndRedraw(true);
            }
        });

        getGenre();
        categoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (genreModel != null && genreModel.result != null && genreModel.result.size() > 0)
                    showCategoryListing(genreModel.result);
            }
        });
    }

    private void getGenre() {
        Log.e("CHANNEL_REQUEST", "Channel List URL ");
        progressBar2.setVisibility(View.VISIBLE);
        StringRequest jsonObjReq = new StringRequest(Request.Method.GET, genreUrl, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "genre response=====>>>> " + response.toString());

                // progressBar2.setVisibility(View.GONE);
                genreModel = GsonContextLoader.getGsonContext().fromJson(response, GenreModel.class);

                try {
                    if (genreModel.code == 1) {
                        ResultGenre result = new ResultGenre();
                        result.id = 0;
                        result.genre_name = "All";
                        genreModel.result.add(0, result);
                    } else {
                        Log.e(TAG, "error in getting data!!!!!!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                progressBar2.setVisibility(View.INVISIBLE);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        ApplicationLoader.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void getChannelList(final EPGData epgData) {
        String url = "http://api-aut.multitvsolution.com/automatorapi/v6/content/getLiveGenre/token/5d09d5cc77d03/device/android/live_limit/100/live_offset/0";
        Log.d("CHANNEL_REQUEST", "Channel List URL ");
        progressBar2.setVisibility(View.VISIBLE);
        StringRequest jsonObjReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "channel response=====>>>> " + response.toString());

                progressBar2.setVisibility(View.GONE);
                channelResponse = GsonContextLoader.getGsonContext().fromJson(response, ChannelModel.class);

                try {
                    if (channelResponse.code == 1) {
                        List<Live> live = channelResponse.result.live;
                        Log.d(TAG, "live size=======" + live.size());
                        for (int i = 0; i < live.size(); i++) {
                            for (int j = 0; j < serverResponse.getResult().getChannels().size(); j++) {
                                if (serverResponse.getResult().getChannels().get(j).getAid().equals(live.get(i).id)) {
                                    serverResponse.getResult().getChannels().get(j).setUrl(live.get(i).url);
                                    serverResponse.getResult().getChannels().get(j).setThumbnail(live.get(i).thumbnail.medium);
                                    serverResponse.getResult().getChannels().get(j).setGenres(live.get(i).genres);
                                }
                            }
                        }
                        String server = GsonContextLoader.getGsonContext().toJson(serverResponse);
                        prefs.setPreferencesString(MainActivity.this, "server", server);

                        epg.setEPGData(epgData);
                        epg.recalculateAndRedraw(false);
                    } else {
                        Log.e(TAG, "error in getting data!!!!!!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                progressBar2.setVisibility(View.INVISIBLE);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

//                params.put("device", "android");

                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        ApplicationLoader.getInstance().addToRequestQueue(jsonObjReq);

    }

    @Override
    protected void onDestroy() {
        if (epg != null) {
            epg.clearEPGImageCache();
        }
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_channel) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void getBaseAndEPGURLs() {
        String url = "http://api-aut.multitvsolution.com/automatorapi/v6/channel/epg/token/5d09d5cc77d03/device/android/offset/0/date/" + formattedDate;//2019-06-18
        Log.d("API_REQUEST", "Base and EPG url : ");
        progressBar2.setVisibility(View.VISIBLE);

//        StringRequest jsonObjReq = new StringRequest(Request.Method.GET,
//                "http://52.220.173.201/PulseApi/api/content/epg?date=" + formattedDate + "&token=5285sd5sdf", new Response.Listener<String>() {
        StringRequest jsonObjReq = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "response::" + response);


                progressBar2.setVisibility(View.GONE);
//                ServerResponse serverResponse = GsonContextLoader.getGsonContext().fromJson(response, ServerResponse.class);
                serverResponse = GsonContextLoader.getGsonContext().fromJson(response, ServerResponseNew.class);
//                prefs.setPreferencesString(MainActivity.this,"server",response);
                try {

                    if (serverResponse.getCode() == 1) {
                        channels = serverResponse.getResult().getChannels();
                        //results = serverResponse.getResult();
                        Log.d(TAG, "channels size=======" + channels.size());
//                        Log.d(TAG, "result size=======" + results.size());


                        new AsyncLoadEPGData(epg, channels).execute();

                    } else {

                        Log.e(TAG, "error in getting data!!!!!!");

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                progressBar2.setVisibility(View.INVISIBLE);

            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

//                params.put("device", "android");

                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        ApplicationLoader.getInstance().addToRequestQueue(jsonObjReq);
    }


    private class AsyncLoadEPGData extends AsyncTask<Void, Void, EPGData> {

        EPG epg;
        ArrayList<Channel> resultList;

        public AsyncLoadEPGData(EPG epg, ArrayList<Channel> resultList) {
            this.epg = epg;
            this.resultList = resultList;
        }

        @Override
        protected EPGData doInBackground(Void... voids) {

            return new EPGDataImpl(MockDataService.getMockData1(resultList, epg));
        }

        @Override
        protected void onPostExecute(EPGData epgData) {
            getChannelList(epgData);
            /*epg.setEPGData(epgData);
            epg.recalculateAndRedraw(false);*/
        }
    }


    private void showCategoryListing(final ArrayList<ResultGenre> result) {

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.setCancelable(false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.customdialog_category);
        ListView categoryList = dialog.findViewById(R.id.categoryList);
        final String server = prefs.getPreferencesString(MainActivity.this, "server");

        CategoryAdapter adapter = new CategoryAdapter(MainActivity.this, R.layout.category_item, result);
        categoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                channels.clear();
                serverResponse = GsonContextLoader.getGsonContext().fromJson(server, ServerResponseNew.class);
                Log.d("pref", serverResponse.getResult().getChannels().size() + "");
                if (position == 0) {
                    channels = serverResponse.getResult().getChannels();
                } else {
                    ArrayList<Channel> chan = new ArrayList<>();
                    for (int i = 0; i < serverResponse.getResult().getChannels().size(); i++) {
                        if (serverResponse.getResult().getChannels().get(i).getGenres().contains(String.valueOf(result.get(position).id))) {
                            chan.add(serverResponse.getResult().getChannels().get(i));
                        }
                    }
                    channels.addAll(chan);
                }

                dialog.dismiss();
                EPGData epgData = new EPGDataImpl(MockDataService.getMockData1(channels, epg));
                epg.setEPGData(epgData);
                epg.recalculateAndRedraw(false);

            }
        });
        String str[] = {"Business News", "Devotional", "Entertainment", "Infotainment", "Kids", "Life style", "Movies", "Music", "News", "Sports"};
        List<String> list = Arrays.asList(str);

        categoryList.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        dialog.show();

    }


    private void showLanguageListing() {
        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.customdialog_category);
        ListView categoryList = (ListView) dialog.findViewById(R.id.categoryList);
        TextView titleTxt = (TextView) dialog.findViewById(R.id.titleTxt);
        titleTxt.setText("Languages");
        String str[] = {"Hindi", "English", "Punjabi", "Marathi", "Assamese", "French", "Gujarati", "Nepali", "Telgu", "Odia", "Tamil", "Urdu", "Malayam"};
        List<String> list = Arrays.asList(str);


//        CategoryAdapter adapter = new CategoryAdapter(MainActivity.this, R.layout.category_item, list);

//        categoryList.setAdapter(adapter);
        dialog.show();

    }


//    http://52.220.173.201/PulseApi/api/content/epg?date=2017-04-12&token=5285sd5sdf&channel=9X%20Jalwa
}
